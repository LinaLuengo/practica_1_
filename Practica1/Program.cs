﻿using System;

namespace Practica1
{

    public struct Empresa
    {
        public String nombreempresa;
        public float ingresos;
        public float gastos;
    }

    class Program
    {
        public static void Ejercicio1() 
        {
            Console.WriteLine("**************** Ejercicio 1 ****************");

            float[] notas = new float[10];
            int aprobados = 0;
            int suspensos = 0;
            int sobresalientes = 0;

            Console.WriteLine("Introduce 10 notas:");
            
            for(int i = 0; i <=9; i++)
            {
                Console.Write("Introduce nota" + Convert.ToString(i + 1) + ": ");
                notas[i] = float.Parse(Console.ReadLine());
            }

            for(int i =0;i <= 9; i++)
            {
                if(notas[i]< 5.0)
                {
                    suspensos++;
                }
                else if(notas[i]>= 9.0)
                {
                    sobresalientes++;
                }
                else
                {
                    aprobados++;
                }
            }

            Console.WriteLine("Número de Suspensos:" + Convert.ToString(suspensos));
            Console.WriteLine("Número de Aprobados:" + Convert.ToString(aprobados));
            Console.WriteLine("Número de Sobresalientes:" + Convert.ToString(sobresalientes));

            Console.ReadLine();
        }


        public static void Ejercicio2()
        {
            Console.WriteLine("**************** Ejercicio 2 ****************");

            int Day, Month, Year, NewMonth, aux;
            string fecha; 

            Console.Write("Introduce la fecha: ");
            fecha = Console.ReadLine();
            
            Day = Int32.Parse(fecha.Substring(0,2));
            Month= Int32.Parse(fecha.Substring(3,2));
            Year = Int32.Parse(fecha.Substring(6,4));

            if (Day <1 || Day >30 || Month >12 || Month < 1)
            {
                Console.WriteLine("No se ha insertado una fecha correctamente");
            }
            else
            {
                NewMonth = Month + 3;
                if (NewMonth > 12)
                {
                    aux = NewMonth - 12;
                    NewMonth = aux;
                    Year = Year + 1;
                }
                Console.WriteLine("La fecha de pago es: " + Day + "/" + NewMonth + "/" + Year);
            }


            Console.ReadLine();
        }

        public static void Ejercicio3()
        {
            Console.WriteLine("**************** Ejercicio 3 ****************");

            int[] numeros = new int[10];
            numeros[0] = 22;
            numeros[1] = 6;
            numeros[2] = 10;
            numeros[3] = 4;
            numeros[4] = 20;
            numeros[5] = 12;
            numeros[6] = 24;
            numeros[7] = 8;
            numeros[8] = 1;
            numeros[9] = 2;

            Console.Write("Los numeros son: ");

            for (int i = 0; i < numeros.Length; i++)
            {
                Console.Write(" " + numeros[i]);
            }

            Console.WriteLine("");
            int j= numeros.Length-1;
            Console.Write("Los numeros al revés son: ");
            while (j >= 0 )
            {
                Console.Write(" " + numeros[j]);
                j--;
            }
            
            Console.ReadLine();
        }

        public static void Ejercicio4()
        {
            Console.WriteLine("**************** Ejercicio 4 ****************");

            Empresa[] Empresas = new Empresa[10];

            float peor_resultado = float.PositiveInfinity;       
            float diferencia = 0;
            int peor_empresa = -1;

           
            for (int i = 0; i < Empresas.Length; i++)
            {
                Console.WriteLine("Introduce nombre de la empresa {0}", i + 1);
                Empresas[i].nombreempresa = Console.ReadLine();
                Console.WriteLine("Introduce ingresos de la empresa {0}", i + 1);
                Empresas[i].ingresos = float.Parse(Console.ReadLine());
                Console.WriteLine("Introduce gastos de la empresa {0}", i + 1);
                Empresas[i].gastos = float.Parse(Console.ReadLine());
            }

             
            for (int i = 0; i < Empresas.Length; i++)
            {
                diferencia = Empresas[i].ingresos - Empresas[i].gastos;
                if (diferencia <= peor_resultado)
                {
                    peor_empresa = i;
                    peor_resultado = diferencia;
                }
            }

            
            Console.WriteLine("");
            Console.WriteLine("La empresa con peor resultado es {0}, con resultado de {1}",
                Empresas[peor_empresa].nombreempresa,
                Empresas[peor_empresa].ingresos - Empresas[peor_empresa].gastos);



            Console.ReadLine();
        }

        public static void Ejercicio5()
        {
            Console.WriteLine("**************** Ejercicio 5 ****************");

            int[] numeros = new int[10];
            int num1;
            int num2;

            Console.WriteLine("Introduzca 10 números enteros.");

            
            for (int i = 0; i < numeros.Length; i++)
            {
                Console.Write("Introduce número {0}: ", i + 1);
                numeros[i] = int.Parse(Console.ReadLine());
            }

           
            if (numeros[0] >= numeros[1])
            {
                num1 = numeros[0];
                num2 = numeros[1];
            }
            else
            {
                num1 = numeros[1];
                num2 = numeros[0];
            }


            for (int i = 2; i < numeros.Length; i++)
            {
                if (numeros[i] >= num1)
                {
                    num2 = num1;
                    num1 = numeros[i];
                    continue;
                }
                if (numeros[i] > num2)
                {
                    num2 = numeros[i];
                }
            }


            Console.WriteLine("Los dos números mayores son {0} y {1}", num1, num2);



            Console.ReadLine();
        }

        public static void Ejercicio6()
        {
            Console.WriteLine("**************** Ejercicio 6 ****************");

            int[] numeros = new int[10];
            int num_auxiliar;
            int distintos = 0;

            Console.WriteLine("Introduzca 10 números enteros.");

            
            for (int i = 0; i < numeros.Length; i++)
            {
                Console.Write("Introduce número {0}: ", i + 1);
                numeros[i] = int.Parse(Console.ReadLine());
            }

            
            Array.Sort(numeros);

            num_auxiliar = numeros[0];
            distintos = 1;

            for (int i = 1; i < numeros.Length; i++)
            {
                if (num_auxiliar != numeros[i])
                {
                    distintos += 1;
                    num_auxiliar = numeros[i];
                }
            }

            Console.WriteLine("Existen {0} distintos", distintos);

            Console.ReadLine();
        }

        public static void Ejercicio7()
        {
            Console.WriteLine("**************** Ejercicio 7 ****************");

            float[] calificaciones = new float[10];
            float nota_media = 0;
            int num_calificaciones = 0;
            int calificacion_inicial;
            int calificacion_final;

            Console.WriteLine("Introduzca 10 calificaciones.");

           
            for (int i = 0; i < calificaciones.Length; i++)
            {
                Console.Write("Introduce calificación {0}: ", i + 1);
                calificaciones[i] = float.Parse(Console.ReadLine());
            }

            
            Array.Sort(calificaciones);

            calificacion_inicial = 0;
            calificacion_final = 9;

            if ((calificaciones[9] - calificaciones[0]) > 3)
            {
                calificacion_inicial = 1;
                calificacion_final = 8;
            }


            for (int i = calificacion_inicial; i <= calificacion_final; i++)
            {
                nota_media = nota_media + calificaciones[i];
                num_calificaciones = num_calificaciones + 1;
            }

            nota_media = nota_media / num_calificaciones;

            Console.WriteLine("La nota media de las calificaciones es: {0}", nota_media);


            Console.ReadLine();
        }


        static void Main(string[] args)
        {
            Ejercicio1();
            Ejercicio2();
            Ejercicio3(); 
            Ejercicio4(); 
            Ejercicio5(); 
            Ejercicio6(); 
            Ejercicio7(); 
        }
    }
}
